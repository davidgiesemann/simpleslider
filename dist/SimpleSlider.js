/**
 * (c) David Giesemann
 * info@david-giesemann.de
 */

function SimpleSlider(options) {
	this.settings = {
		element: options.element || null,
		arrows: options.arrows || false,
		dots: options.dots || false,
		onInit: options.onInit || null,
		onNext: options.onNext || null,
		onPrev: options.onPrev || null,
		onUpdatePosition: options.onUpdatePosition || null
	};

	this.init();
}

SimpleSlider.prototype.init = function () {
	if (this.settings.element) {
		var element = this.settings.element,
			slides = document.createElement('div'),
			arrows = document.createElement('div'),
			dots = document.createElement('div');

		this.slides = slides.children;

		while (element.firstChild) slides.appendChild(element.firstChild);
		slides.className = 'slides';

		element.className += ' simpleSlider';
		element.appendChild(slides);

		if (this.settings.arrows) {
			var next = document.createElement('button'),
				prev = document.createElement('button');

			this.next = this.next.bind(this);
			this.prev = this.prev.bind(this);

			next.type = 'button';
			next.className = 'next';
			next.addEventListener('click', this.next);

			prev.type = 'button';
			prev.className = 'prev';
			prev.addEventListener('click', this.prev);

			arrows.className = 'arrows';
			arrows.appendChild(next);
			arrows.appendChild(prev);

			element.appendChild(arrows);
		}

		if (this.settings.dots) {
			dots.className = 'dots';

			for (var i = 0; i < this.slides.length; i++) {
				var dot = document.createElement('button');

				dot.type = 'button';
				dot.setSlideIndex = this.setCurrentSlideIndex.bind(this, i);
				dot.addEventListener('click', dot.setSlideIndex);

				dots.appendChild(dot);
			}

			element.appendChild(dots);
		}

		this.setCurrentSlideIndex(0);
	}

	if (this.settings.onInit) this.settings.onInit(this);
};

SimpleSlider.prototype.setCurrentSlideIndex = function (index) {
	this.currentSlideIndex = index;

	if (this.settings.dots) {
		var dots = this.settings.element.querySelectorAll('.dots button');

		for (var i = 0; i < this.slides.length; i++) {
			if (i === index) {
				if (dots[i]) dots[i].className = 'active';
			} else {
				if (dots[i]) dots[i].removeAttribute('class');
			}
		}
	}

	this.updatePosition();
};

SimpleSlider.prototype.next = function () {
	if (this.currentSlideIndex < this.slides.length - 1) {
		this.setCurrentSlideIndex(this.currentSlideIndex + 1);
	} else {
		this.setCurrentSlideIndex(0);
	}

	if (this.settings.onNext) this.settings.onNext(this);
};

SimpleSlider.prototype.prev = function () {
	if (this.currentSlideIndex > 0) {
		this.setCurrentSlideIndex(this.currentSlideIndex - 1);
	} else {
		this.setCurrentSlideIndex(this.slides.length - 1);
	}

	if (this.settings.onPrev) this.settings.onPrev(this);
};

SimpleSlider.prototype.updatePosition = function () {
	var slides = this.settings.element.querySelector('.slides'),
		percentage = this.currentSlideIndex * 100;

	slides.style.transform = 'translateX(-' + percentage + '%)';

	if (this.settings.onUpdatePosition) this.settings.onUpdatePosition(this);
};
