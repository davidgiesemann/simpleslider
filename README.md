# SimpleSlider #

Ein simpler javascript-Slider ohne Abhängigkeiten

### Installation ###

* Script & Styles hinzufügen
```html
    <link rel="stylesheet" href="dist/SimpleSlider.css" />
    <script defer src="dist/SimpleSlider.min.js"></script>
```
* Konfiguration
```javascript
    new SimpleSlider({
        element: document.getElementById('slider'),
        dots: true,
        arrows: true,
        onInit: function(slider){ console.log('Init: ', slider) },
        onNext: function(slider){ console.log('Next: ', slider) },
        onPrev: function(slider){ console.log('Prev: ', slider) },
        onUpdatePosition: function(slider){ console.log('UpdatePosition: ', slider) }
    });
```

### Kontakt ###

David Giesemann [info@david-giesemann.de](mailto:info@david-giesemann.de)
